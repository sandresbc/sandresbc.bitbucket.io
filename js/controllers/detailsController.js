pokemon.controller('detailsController', ['$location', '$scope', '$log', '$window', '$routeParams', 'pokemonService', '$http', function($location, $scope, $log, $window, $routeParams, pokemonService, $http) {

	$scope.pokemon_id = $routeParams.pokeid;
	$scope.loading_pokemon = true;
	$scope.evolutions = [];
	$scope.loading_evol = true;

	// get pokemon details
	pokemonService.getPokemon($scope.pokemon_id).then(
		function(result){
			if (result.data) {
				$scope.pokemon_details = result.data;
				getSpecie();
			}else{
				console.error("error loading pokemons", result);
			}
		}
	, function(error){
		console.error("error loading pokemons", error);
	});

	var getSpecie = function(){
		pokemonService.getPokemonSpecie($scope.pokemon_id).then(
			function(result){
				if (result.data) {
					$scope.specie = result.data;
					getEvolution();
				}else{
					console.error("error loading species", result);
					$scope.loading_evol = false;
					$scope.loading_pokemon = false;
				}
			}
		, function(error){});
	}

	$scope.getPokemonId = function(url){
		var url_content = url.split("/");
		return url_content[ url_content.length - 2 ];
	}

	var getEvolution = function(){
		pokemonService.getPokeURLdata($scope.specie.evolution_chain.url).then(
			function(result){
				if (result.status == 200) {
					$scope.evolutions.push(result.data.chain);
					console.log("evolution", $scope.evolutions)
					$scope.loading_evol = false;
				}else{
					console.error("error loading pokemons", response);
				}
				$scope.loading_pokemon = false;
			}
		, function(error){});
	}
	
	$scope.getText =  function(texts){
		if(!texts) return;
		var text = texts.filter( function(text){
			return text.language.name == 'en';
		});
		return text[0].flavor_text;
	}

	$scope.getTypes = function(detail_types){
		if(!detail_types) return;
		var types = '';
		for (var i = 0; i < detail_types.length; i++) {
			var type = detail_types[i];
			if(i+1 < detail_types.length){
				types+= type.type.name+', ';
			}else{
				types+= type.type.name;
			}
			
		}
		return types;
	}

	$scope.getAbilities = function(abilities){
		if(!abilities) return;
		var types = '';
		for (var i = 0; i < abilities.length; i++) {
			var ability = abilities[i];
			if(i+1 < abilities.length){
				types+= ability.ability.name+', ';
			}else{
				types+= ability.ability.name;
			}
			
		}
		return types;
	}


	$scope.getEvolPic =  function(pokemon){
		var id = $scope.getPokemonId(pokemon.species.url);
		return 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/'+id+'.png';
	}



}]);