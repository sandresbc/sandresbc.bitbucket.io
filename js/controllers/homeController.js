pokemon.controller('homeController', ['$location', '$scope', '$log', '$window', '$routeParams', 'pokemonService', '$http','$rootScope', function($location, $scope, $log, $window, $routeParams, pokemonService,$http,$rootScope) {

	
	$scope.loading_pokemons = false;
	$scope.next_url = 'https://pokeapi.co/api/v2/pokemon/';
	$scope.prev_url = null;
	$scope.all_pokemons = []

	$scope.getPokemonId = function(pokemon){
		var url_content = pokemon.url.split("/");
		return url_content[ url_content.length - 2 ];
	}

	$scope.nextPokemonList = function(){
		if($scope.loading_pokemons || !$scope.next_url) return;
		$scope.loading_pokemons = true;
		pokemonService.getPokeURLdata($scope.next_url).then(
			function(response){
				$scope.loading_pokemons = false;
				if (response.status == 200) {
					$scope.all_pokemons.concat(response.data.results);

					var items = response.data.results;

					for (var i = 0; i < items.length; i++) {
						$scope.all_pokemons.push(items[i]);
					}

					$scope.next_url = response.data.next;
					$scope.prev_url	= response.data.previous;
				}else{
					console.error("error loading pokemons", response);
				}
	
			}
		, function(error){});
	}



}]);