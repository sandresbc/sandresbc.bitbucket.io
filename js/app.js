var pokemon = angular.module('pokemon', ['ngRoute','infinite-scroll']);

pokemon.config(['$routeProvider','$locationProvider', '$rootScopeProvider',function($routeProvider, $locationProvider, $rootScopeProvider) {

	$routeProvider
	.when('/', {
	  redirectTo: '/',
	  templateUrl: 'views/home.html',
	  controller: 'homeController'
	})
	.when('/details/:pokeid', {
	  templateUrl: 'views/details.html',
	  controller: 'detailsController',
	  'class': 'details'
	})
	.otherwise({redirectTo: '/'});

}]).run(run);

run.$inject = ['$rootScope','$location','$http'];

function run($rootScope,$location,$http) {

}

pokemon.directive('classRoute', function($rootScope, $route) {
	return function(scope, elem, attr) {
		var previous = '';
		$rootScope.$on('$routeChangeSuccess', function(event, currentRoute) {
			var route = currentRoute.$$route;
			if(route) {

				var cls = route['class'];

				if(previous) {
					attr.$removeClass(previous);
				}

				if(cls) {
					previous = cls;
					attr.$addClass(cls);
				}
			}
		});
	};

});

pokemon.filter('reverse', function() {
	return function(items) {
	  return items.slice().reverse();
	};
  });

