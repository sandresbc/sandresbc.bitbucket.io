pokemon.service('pokemonService', pokemonService);

pokemonService.$inject = ['$http'];

function pokemonService($http) {
	var service = {};
	service.getAll = getAll;
	service.getPokemon = getPokemon;
	service.getPokeURLdata = getPokeURLdata;
	service.getPokemonSpecie = getPokemonSpecie;
	return service;

	function getAll(){
		return $http.get('https://pokeapi.co/api/v2/pokemon/').then(handleSuccess, handleError('Error getting all pokemons'));
  }

  function getPokemon(id){
		return $http.get('https://pokeapi.co/api/v2/pokemon/'+id).then(handleSuccess, handleError('Error getting pokemon info'));
  }

  function getPokemonSpecie(id){
	return $http.get('https://pokeapi.co/api/v2/pokemon-species/'+id+'/').then(handleSuccess, handleError('Error getting pokemon info'));
  }

  function getPokeURLdata(url){
	return $http.get(url).then(handleSuccess, handleError('Error getting pokemon info'));
  }

	function handleSuccess(data) {
		return data;
	}

	function handleError(error) {
		return function () {
			return { success: false, message: error };
		};
	}
}
